#ifndef SERIALPORTREADER_H
#define SERIALPORTREADER_H

#include <QByteArray>
#include <QSerialPort>
#include <QTextStream>
#include <QTimer>
#include <QCoreApplication>
#include <QDebug>

QT_BEGIN_NAMESPACE

QT_END_NAMESPACE

class SerialPortReader : public QObject
{
    Q_OBJECT
private:
    QSerialPort *m_serialPort = nullptr;
    QByteArray m_readData;
public:
    explicit SerialPortReader(QSerialPort *serialPort, QObject *parent = nullptr) :
        QObject(parent),
        m_serialPort(serialPort)
    {
        connect(m_serialPort, &QSerialPort::readyRead, this, &SerialPortReader::handleReadyRead);
        connect(m_serialPort, &QSerialPort::errorOccurred, this, &SerialPortReader::handleError);
    }

    bool hasCommand(){
        for(auto &data : m_readData){
            if(data=='\r') return true;
        }
        return false;
    }

    QString getCommand(){
        QString out ="";
        while(m_readData[0]!='\r'){
            out.append(m_readData.at(0));
            m_readData.remove(0,1);
        }
        m_readData.remove(0,2);
        return out;
    }

private slots:
    void handleReadyRead()
    {
        //qDebug() << m_serialPort->readAll();
        m_readData.append(m_serialPort->readAll());
    }

    void handleError(QSerialPort::SerialPortError error)
    {
        if (error == QSerialPort::ReadError) {
            qDebug() << QObject::tr("An I/O error occurred while reading "
                                            "the data from port %1, error: %2")
                                .arg(m_serialPort->portName())
                                .arg(m_serialPort->errorString())
                             << "\n";
            QCoreApplication::exit(1);
        }
    }

};

#endif // SERIALPORTREADER_H
