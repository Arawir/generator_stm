#include "mainwindow.h"
#include <QApplication>

#include "serialportreader.h"
#include <QCoreApplication>
#include <QSerialPort>
#include <QStringList>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QSerialPort serialPort;
    serialPort.setPortName("/dev/ttyACM0");
    serialPort.setBaudRate(115200);
    SerialPortReader serialPortReader(&serialPort);

    QApplication a(argc, argv);
    MainWindow w(&serialPortReader);
    w.show();



    if (!serialPort.open(QIODevice::ReadOnly)) {
        qDebug() <<"BLAD!!!!!!!!!!!";
//        standardOutput << QObject::tr("Failed to open port /dev/ttyACM0, error: %2")
//                          .arg(serialPort.errorString())
//                       << "\n";
        return 1;
    }

    ////////////////////////////

    return a.exec();
}
