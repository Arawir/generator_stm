#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include "ui_mainwindow.h"

#include "engine.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(SerialPortReader *newComm,QWidget *parent = 0) :
        QMainWindow(parent),
        ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        mainLayout = new MainLayout(newComm);
        setCentralWidget(mainLayout);
    }
    ~MainWindow()
    {
        delete ui;
    }

private:
    Ui::MainWindow *ui;
    MainLayout *mainLayout;
};

#endif // MAINWINDOW_H
