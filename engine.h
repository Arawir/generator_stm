#ifndef ENGINE
#define ENGINE

#include "osciloscope.h"



class MainLayout : public QWidget
{
public:

    explicit MainLayout(SerialPortReader *newComm)
    {
        layout1 = new QHBoxLayout{};
        setLayout(layout1);

        layout2 = new QVBoxLayout{};
        layout1->addLayout(layout2);

        layout3 = new QVBoxLayout{};
        layout1->addLayout(layout3);

        feedbackLabel = new QLabel("Feedback");
        layout2->addWidget(feedbackLabel);

        feedbackLine = new QLineEdit();
        feedbackLine->setReadOnly(true);
        layout2->addWidget(feedbackLine);

        typeLabel = new QLabel("Select signal type");
        layout2->addWidget(typeLabel);

        typeSelector = new QComboBox();
        typeSelector->addItem("None");
        typeSelector->addItem("Sin");
        typeSelector->addItem("Tri");
        typeSelector->addItem("Rect");
        layout2->addWidget(typeSelector);

        amplLabel = new QLabel("Select amplitude x.xxx[V]");
        layout2->addWidget(amplLabel);

        amplEdit = new QLineEdit();
        layout2->addWidget(amplEdit);

        freqLabel = new QLabel("Select frequency xxx.xxx[Hz]");
        layout2->addWidget(freqLabel);

        freqEdit = new QLineEdit();
        layout2->addWidget(freqEdit);

        updateButton = new QPushButton("Update");
        layout2->addWidget(updateButton);


        timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this, &MainLayout::cycle);
        timer->start(1000);


        osciloscope = new Osciloscope{newComm};
        layout1->addWidget(osciloscope);
    }



private slots:
    void cycle(){
        QString lastFCommand = feedbackLine->text();
        while(osciloscope->comm->hasCommand()){
            QString command = osciloscope->comm->getCommand();
            if(command[0]=="F") lastFCommand = command;
            if(command[0]=="A"){
                if(osciloscope->adcCollect){
                    osciloscope->adc[osciloscope->adcIter] = ((command.at(1).toLatin1()-48)*256+(command[2].toLatin1()-48)*16+(command[3].toLatin1()-48));
                    osciloscope->adcIter++;
                    osciloscope->updateChart();
                    if(osciloscope->adcIter>=(osciloscope->adcPoints)){
                        osciloscope->adcCollect=false;
                        osciloscope->updateFft();
                    }
                }
            }
        }
        feedbackLine->setText(lastFCommand);
    }


    QHBoxLayout *layout1;
    QVBoxLayout *layout2;
    QVBoxLayout *layout3;

    QLabel *feedbackLabel;
    QLineEdit *feedbackLine;

    QLabel *typeLabel;
    QLabel *freqLabel;
    QLabel *amplLabel;
    QLineEdit *freqEdit;
    QLineEdit *amplEdit;
    QComboBox *typeSelector;
    QPushButton *updateButton;

    QTimer *timer;

    Osciloscope *osciloscope;
};


#endif // ENGINE

