#ifndef OSCILOSCOPE
#define OSCILOSCOPE

#include "serialportreader.h"
#include <QTimer>
#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QVector>
#include <QValueAxis>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

#include <complex>
#include <iostream>
#include <valarray>

#define im std::complex<double>{0.0,1.0}

const double PI = 3.141592653589793238460;

typedef std::complex<double> cpx;
typedef std::valarray<cpx> CArray;
typedef std::vector<cpx> cvec;

QT_CHARTS_USE_NAMESPACE

class Osciloscope : public QWidget{
public:
    QVBoxLayout *layout1;
    QHBoxLayout *layout2;

    QLineEdit *adcTimeEdit;

    QVector<int> adc;
    int adcIter=0;
    double adcT = 0.1;
    double adcTimeInS = 10;
    bool adcCollect = false;
    int adcPoints = 10;
    int adcPointsAdded=0;

    QChart *chart;
    QChartView *chartView;
    QLineSeries *series;
    QPushButton *updateChartButton;

    QChart *fourierChart;
    QChartView *fourierChartView;
    QLineSeries *fourierSeriesR;
    QLineSeries *fourierSeriesI;

    double omegaMin=-5.0;
    double omegaMax= 5.0;
    int omegaN = 101;
    std::vector<double> omega;
public:

    SerialPortReader *comm;

public:
    explicit Osciloscope(SerialPortReader *newComm) :
        comm{newComm}
    {

        layout1 = new QVBoxLayout{};
        setLayout(layout1);

        layout2 = new QHBoxLayout{};
        layout1->addLayout(layout2);


        series = new QLineSeries();
        series->append(0, 0);
        series->append(2, 4);
        series->append(3, 8);
        series->append(7, 4);
        series->append(20, 500);

        chart = new QChart();
        chart->legend()->hide();
        chart->addSeries(series);
        chart->createDefaultAxes();
        chart->setTitle("Oscilloscope");

        chartView = new QChartView(chart);
        chartView->setRenderHint(QPainter::Antialiasing);

        adcTimeEdit = new QLineEdit("3");
        layout2->addWidget(adcTimeEdit);

        updateChartButton = new QPushButton("Update chart");
        connect(updateChartButton, &QPushButton::pressed, this, &Osciloscope::startMeasurement);
        layout2->addWidget(updateChartButton);

        layout1->addWidget(chartView);


        fourierSeriesR = new QLineSeries();
        fourierSeriesI = new QLineSeries();
        fourierSeriesR->append(0, 0);
        fourierSeriesR->append(2, 4);
        fourierSeriesR->append(3, 8);
        fourierSeriesR->append(100, 4);
        fourierSeriesR->append(20, 500);

        fourierChart = new QChart();
        fourierChart->legend()->hide();
        fourierChart->addSeries(fourierSeriesR);
        fourierChart->addSeries(fourierSeriesI);
        fourierChart->createDefaultAxes();
        fourierChart->setTitle("Fourier space");

        fourierChartView = new QChartView(fourierChart);
        fourierChartView->setRenderHint(QPainter::Antialiasing);
        layout1->addWidget(fourierChartView);

        prepareFFT();
    }

    void prepareFFT(){
        omega.resize(omegaN);
        double omegaD = (omegaMax-omegaMin)/((double)(omegaN-1));
        for(int i=0; i<omegaN; i++){
            omega[i]=((double)i)*omegaD+omegaMin;
        }


    }

    void updateChart(){
        for(int i=adcPointsAdded; i<adcIter; i++){
            series->append(adcT*(double)i,adc[i]);
            adcPointsAdded++;
        }

        chart->update();
    }

    void updateFft(){
        cvec data;

        for(int i=0;i<adc.size();i++){
            data.push_back((cpx)adc[i]);
        }

        cvec out = fft(data);

        fourierSeriesR->clear();
        fourierSeriesI->clear();

        for (int i = 0; i < out.size(); i++){
            fourierSeriesR->append(omega[i],out[i].real());
            fourierSeriesI->append(omega[i],out[i].imag());
        }

        double minY = std::min(findVal(fourierSeriesR,"y","min"),findVal(fourierSeriesI,"y","min"));
        double minX = std::min(findVal(fourierSeriesR,"x","min"),findVal(fourierSeriesI,"x","min"));
        double maxY = std::max(findVal(fourierSeriesR,"y","max"),findVal(fourierSeriesI,"y","max"));
        double maxX = std::max(findVal(fourierSeriesR,"x","max"),findVal(fourierSeriesI,"x","max"));

        fourierChart->axisY()->setRange(minY,maxY);
        fourierChart->axisX()->setRange(minX,maxX);

        fourierChart->update();
    }

    double findVal(QLineSeries *data, QString xy, QString minmax){
        double out;
        if(xy=="x"){
            out = data->at(0).x();
            for(int i=0; i<data->count(); i++){
                if(minmax=="min"){ if(data->at(i).x() < out){ out = data->at(i).x(); } }
                if(minmax=="max"){ if(data->at(i).x() > out){ out = data->at(i).x(); } }
            }
        }
        if(xy=="y"){
            out = data->at(0).y();
            for(int i=0; i<data->count(); i++){
                if(minmax=="min"){ if(data->at(i).y() < out){ out = data->at(i).y(); } }
                if(minmax=="max"){ if(data->at(i).y() > out){ out = data->at(i).y(); } }
            }
        }

        return out;
    }

    cvec fft(cvec &x){
        cvec X(omegaN,0.0);



        for(int ik=0; ik<omegaN; ik++){
            X[ik] += x[0];
            for(int n=1; n<x.size(); n++){
                cpx a = -2.0*M_PI*omega[ik]*((double)n)/((double)x.size());
                cpx b = exp(im*a);
                X[ik] += x[n]*b;
            }
        }
        return X;
    }

    void ifft(cvec& x){
        for(uint i=0; i<x.size(); i++){
            x[i] = std::conj(x[i]);
        }
       // x = x.apply(std::conj);
        fft( x );
        //x = x.apply(std::conj);
        for(uint i=0; i<x.size(); i++){
            x[i] = std::conj(x[i]);
        }

        for(uint i=0; i<x.size(); i++){
            x[i] /= x.size();
        }

        //x /= x.size();
    }

private slots:
    void startMeasurement(){
        adcIter=0;
        adcPointsAdded=0;
        adcPoints = (int)(adcTimeEdit->text().toDouble()/adcT)+1;
        adc.resize(adcPoints);
        adcCollect=true;
        chart->axisX()->setRange(0.0,adcTimeEdit->text().toDouble());

        series->clear();
        chart->update();
    }

};

#endif // OSCILOSCOPE

